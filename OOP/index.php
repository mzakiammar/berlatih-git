<?php

require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new Animal("Shaun");

echo "Nama Hewan : " . $sheep->hewan . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $sheep->legs . "<br>"; // 4
echo "Apakah Hewan Berdarah Dingin : " . $sheep->cold_blooded . "<br><br>"; // "no"

$sungokong = new Ape("Kera Sakti");

echo "Nama Hewan : " . $sungokong->hewan . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $sungokong->legs . "<br>"; // 4
echo "Apakah Hewan Berdarah Dingin : " . $sungokong->cold_blooded . "<br>"; // "no"
echo "Yell : " . $sungokong->yell . "<br><br>";

$kodok = new Frog("Buduk");

echo "Nama Hewan : " . $kodok->hewan . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $kodok->legs . "<br>"; // 4
echo "Apakah Hewan Berdarah Dingin : " . $kodok->cold_blooded . "<br>"; // "no"
echo "Yell : " . $kodok->jump . "<br>";


?>