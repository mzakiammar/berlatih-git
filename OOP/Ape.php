<?php
require_once('Animal.php');

class Ape extends Animal

{
    public $legs = 2;
    public $cold_blooded = "no";
    public $yell = "Auooo";
    public $hewan;

    public function __construct($nama)
    {
        $this->hewan = $nama;
    }


}

?>