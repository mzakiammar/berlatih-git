<?php
require_once('Animal.php');

class Frog extends Animal

{
    public $legs = 2;
    public $cold_blooded = "no";
    public $jump = "Hop Hop";
    public $hewan;

    public function __construct($nama)
    {
        $this->hewan = $nama;
    }

}

?>